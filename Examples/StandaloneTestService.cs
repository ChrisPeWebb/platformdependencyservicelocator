﻿using UnityEngine;
using CUL.IOC;

[DependencyService(
    typeof(ITestService), 
    RuntimePlatform.WindowsEditor, RuntimePlatform.WindowsPlayer,
    RuntimePlatform.OSXEditor, RuntimePlatform.OSXPlayer,
    RuntimePlatform.LinuxPlayer)]
public class StandaloneTestService : ITestService
{
    public string GetSomeString()
    {
        return "I come from the standalone service";
    }
}
