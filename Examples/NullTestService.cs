﻿using CUL.IOC;

[DependencyService(typeof(ITestService))]
public class NullTestService : ITestService
{
    public string GetSomeString()
    {
        return "";
    }
}
