﻿#if UNITY_PS4

using UnityEngine;
using CUL.IOC;

[DependencyService(
    typeof(ITestService), 
    RuntimePlatform.PS4)]
public class PS4TestService : ITestService
{
    public string GetSomeString()
    {
        return "I come from the PS4 service";
    }
}

#endif
