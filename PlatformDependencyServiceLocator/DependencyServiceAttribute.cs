﻿using System;
using UnityEngine;

namespace CUL.IOC
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DependencyServiceAttribute : System.Attribute
    {
        public readonly Type ServiceType;
        public readonly RuntimePlatform[] Platforms;

        public DependencyServiceAttribute(Type a_serviceType, params RuntimePlatform[] a_platforms)
        {
            Platforms = a_platforms;
            ServiceType = a_serviceType;
        }
    }
}
