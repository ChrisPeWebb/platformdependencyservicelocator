﻿using System;
using System.Reflection;
using System.Collections.Generic;

using UnityEngine;


namespace CUL.IOC
{
    public static class PlatformDependencyServiceLocator
    {
        private static Dictionary<Type, object> s_services = new Dictionary<Type, object>();
        private static Dictionary<Type, Type> s_platformDependencyServices = new Dictionary<Type, Type>();
        private static Dictionary<Type, Type> s_nullPlatformDependencyServices = new Dictionary<Type, Type>();


        static PlatformDependencyServiceLocator()
        {
            Initialize();
        }


        public static T Get<T>()
        {
            // If the service already exists, return it
            object service = null;
            if(s_services.TryGetValue(typeof(T), out service))
            {
                return (T)service;
            }

            // If the service can be created, create it, cache it then return it
            Type serviceType;

            if (s_platformDependencyServices.TryGetValue(typeof(T), out serviceType))
            {
                // Create new
                T serviceInstance = (T)Activator.CreateInstance(serviceType);

                //Cache
                s_services.Add(typeof(T), serviceInstance);

                return serviceInstance;
            }

            // Attempt to use the null service for the type
            if (s_nullPlatformDependencyServices.TryGetValue(typeof(T), out serviceType))
            {
                // Create new
                T serviceInstance = (T)Activator.CreateInstance(serviceType);

                //Cache
                s_services.Add(typeof(T), serviceInstance);

                return serviceInstance;
            }

            Debug.LogErrorFormat("Could not resolve service: {0} on current platform: {1}", typeof(T), Application.platform);

            // If we cannot resolve the service, return default
            return default(T);
        }


        private static void Initialize()
        {
            RuntimePlatform currentPlatform = Application.platform;

            //
            // Collect all services for the current platform
            //
            List<Type> dependencyServiceTypes = new List<Type>(10);

            Assembly assembly = Assembly.GetExecutingAssembly();
            foreach (Type type in assembly.GetTypes())
            {
                DependencyServiceAttribute[] serviceAttributes 
                    = (DependencyServiceAttribute[])type.GetCustomAttributes(typeof(DependencyServiceAttribute), true);

                for(int i = 0; i < serviceAttributes.Length; ++i)
                {
                    // If the service attribute states that the service can fullfill this platform, store it
                    for(int j = 0; j < serviceAttributes[i].Platforms.Length; j++)
                    {
                        if(serviceAttributes[i].Platforms[j] == currentPlatform)
                        {
                            if(!s_platformDependencyServices.ContainsKey(serviceAttributes[i].ServiceType))
                            {
                                s_platformDependencyServices.Add(serviceAttributes[i].ServiceType, type);
                                Debug.Log("Interface " + serviceAttributes[i].ServiceType + " fullfilled by type " + type);
                            }
                            else
                            {
                                Debug.LogErrorFormat("Failed to register type: {0} as dependency service for interface: {1}. Has already been registered by type: {2}",
                                    type, serviceAttributes[i].ServiceType, s_platformDependencyServices[serviceAttributes[i].ServiceType]);
                            }
                        }
                    }

                    // If the type is for no particular platform, it is the null/default service
                    if(serviceAttributes[i].Platforms.Length == 0)
                    {
                        if (!s_nullPlatformDependencyServices.ContainsKey(serviceAttributes[i].ServiceType))
                        {
                            s_nullPlatformDependencyServices.Add(serviceAttributes[i].ServiceType, type);
                            Debug.Log("Interface " + serviceAttributes[i].ServiceType + " fullfilled by null/default type " + type);
                        }
                        else
                        {
                            Debug.LogErrorFormat("Failed to register type: {0} as null/default dependency service for interface: {1}. Has already been registered by type: {2}",
                                type, serviceAttributes[i].ServiceType, s_nullPlatformDependencyServices[serviceAttributes[i].ServiceType]);
                        }
                    }
                }
            }

            Debug.Log("Services found for current platform: " + s_platformDependencyServices.Count);
            Debug.Log("Null/Default services found: " + s_nullPlatformDependencyServices.Count);
        }
    }
}